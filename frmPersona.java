package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class frmPersona extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNombres;
	private JLabel lblApellidos;
	private JLabel lblEdad;
	private JLabel lblPeso;
	private JTextField txtNombres;
	private JTextField txtApellidos;
	private JTextField txtEdad;
	private JTextField txtPeso;
	private JButton btnGuardar;
	private JButton btnNuevo;
	private JButton btnCerrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmPersona frame = new frmPersona();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmPersona() {
		setTitle("Registro de personas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 245);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNombres = new JLabel("Nombres");
		lblNombres.setBounds(24, 33, 57, 21);
		contentPane.add(lblNombres);
		
		lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(24, 65, 57, 21);
		contentPane.add(lblApellidos);
		
		lblEdad = new JLabel("Edad");
		lblEdad.setBounds(24, 100, 46, 14);
		contentPane.add(lblEdad);
		
		lblPeso = new JLabel("Peso");
		lblPeso.setBounds(24, 131, 46, 14);
		contentPane.add(lblPeso);
		
		txtNombres = new JTextField();
		txtNombres.setBounds(91, 33, 176, 20);
		contentPane.add(txtNombres);
		txtNombres.setColumns(10);
		
		txtApellidos = new JTextField();
		txtApellidos.setBounds(91, 65, 176, 20);
		contentPane.add(txtApellidos);
		txtApellidos.setColumns(10);
		
		txtEdad = new JTextField();
		txtEdad.setBounds(91, 97, 86, 20);
		contentPane.add(txtEdad);
		txtEdad.setColumns(10);
		
		txtPeso = new JTextField();
		txtPeso.setBounds(91, 128, 86, 20);
		contentPane.add(txtPeso);
		txtPeso.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(this);
		btnGuardar.setBounds(324, 32, 89, 23);
		contentPane.add(btnGuardar);
		
		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(this);
		btnNuevo.setBounds(324, 64, 89, 23);
		contentPane.add(btnNuevo);
		
		btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(this);
		btnCerrar.setBounds(324, 96, 89, 23);
		contentPane.add(btnCerrar);
	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnCerrar) {
			actionPerformedBtnCerrar(arg0);
		}
		if (arg0.getSource() == btnNuevo) {
			actionPerformedBtnNuevo(arg0);
		}
		if (arg0.getSource() == btnGuardar) {
			actionPerformedBtnGuardar(arg0);
		}
	}
	protected void actionPerformedBtnGuardar(ActionEvent arg0) {
		
		
	}
	protected void actionPerformedBtnNuevo(ActionEvent arg0) {
		
		
	}
	protected void actionPerformedBtnCerrar(ActionEvent arg0) {
		
	}
}
