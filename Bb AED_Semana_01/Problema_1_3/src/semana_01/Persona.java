package semana_01;

public class Persona {

  //Atributos
	public String nombre;
	public String apellido;
	public int edad;
	public double estatura;
	public double peso;
	
  //M�todos	
	public String obtenerEstado(){
		
		if(edad >=18)
			return "Mayor de Edad";
		else
			return "Menor de Edad";
	}
	
	public double calcularIMC(){
		return peso/(estatura * estatura);
	}
	
}
