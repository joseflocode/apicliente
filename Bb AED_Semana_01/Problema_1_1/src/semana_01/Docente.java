package semana_01;

public class Docente {

	//Atributos
	public int codigo;
	public String nombre;
	public int horasTrabajadas;
	public double tarifaHoraria;
	
	//M�todos
	public double calcularSueldoBruto(){
		return horasTrabajadas * tarifaHoraria;	
	}
	
	
	public double calcularDescuento(){
		double sueldoBruto= calcularSueldoBruto();
		double descuento;
		
		if( sueldoBruto < 4500 )
			descuento= 0.12 * sueldoBruto;
		else
			if(sueldoBruto <6500)
				descuento= 0.14 * sueldoBruto;
			else
				descuento= 0.16 *sueldoBruto;
		
		return descuento;
	}
	
	public double calcularSueldoNeto(){
		return calcularSueldoBruto() - calcularDescuento();
	}
	
	
}
